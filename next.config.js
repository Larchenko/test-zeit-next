require('dotenv').config();

const withPlugins = require('next-compose-plugins');


const nextConfig = {
  generateBuildId: async () => 'test-next-zeit@0.1.0',
  webpack(config) {
    

    config.plugins = config.plugins || [];

    

    return config;
  },
  env: {
    REACT_APP_SERVER_URL: process.env.REACT_APP_SERVER_URL,
    REACT_APP_API_URL: process.env.REACT_APP_API_URL
  }
};

module.exports = withPlugins(
  [],
  nextConfig
);
