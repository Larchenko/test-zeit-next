import axios from './axios';

export const fetchActivitiesAndEmployees = async ({
  date,
  offset,
  days,
  includeEmployees
}) =>
  axios
    .get('planning/activities', {
      params: {
        date,
        offset,
        days,
        include_employees: includeEmployees
      }
    })
    .then(({ data }) => data);

export const fetchOnlyActivities = async ({ date, offset, days }) =>
  fetchActivitiesAndEmployees({ date, offset, days, includeEmployees: false });

export const fetchWorkOrders = async () =>
  axios.get('non-invoiced-work-orders/').then(({ data }) => data);

export const postActivity = async formData =>
  axios.post('planning/activities/save/', formData).then(({ data }) => data);

export const deleteActivity = async id =>
  axios.post('planning/activities/delete/', { id });
