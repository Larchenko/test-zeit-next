import axiosLib from 'axios';

const axios = axiosLib.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'X-Api-Key': 'ee41fc57-0d0e-4291-a347-4a25caff7d2d'
  }
});

export default axios;
